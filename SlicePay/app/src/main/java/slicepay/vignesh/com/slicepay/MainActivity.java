package slicepay.vignesh.com.slicepay;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import slicepay.vignesh.com.slicepay.response.Photo;
import slicepay.vignesh.com.slicepay.response.SearchResponse;
import slicepay.vignesh.com.slicepay.restclient.FlikrAPIClient;
import slicepay.vignesh.com.slicepay.restclient.helper.ServiceHelper;
import slicepay.vignesh.com.slicepay.util.CustomVolleyRequestQueue;
import slicepay.vignesh.com.slicepay.util.EndlessRecyclerOnScrollListener;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,View.OnClickListener {

    RecyclerView imageGridView;
    List flikrPhotosList = new ArrayList();
    RelativeLayout apiLayout;
    EditText apiEditText;
    Button apiButton;
    String APIKey="9f89151d82e427401680cd48dd2d5cf5";
    int page=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        flikrAPICall();
        imageGridView=findViewById(R.id.imageGridView);
        apiButton=findViewById(R.id.api_button);
        apiLayout=findViewById(R.id.api_layout);
        apiEditText=findViewById(R.id.api_edit_text);

        imageGridView.setLayoutManager(new GridLayoutManager(this,3));

        apiButton.setOnClickListener(this);
        imageGridView.setHasFixedSize(true);
        final ImageAdapter imageAdapter=new ImageAdapter(flikrPhotosList);
        imageGridView.setAdapter(imageAdapter);

        imageGridView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                page++;
                flikrAPICall();
            }
        });


    }

    public void flikrAPICall()
    {
        final FlikrAPIClient flikrAPIClient = ServiceHelper.createService(FlikrAPIClient.class);
        Observable<Response<SearchResponse>> observable = flikrAPIClient
                .search("flickr.interestingness.getList",page,APIKey,5,"json",1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        Subscription subscription = observable.subscribe(new PhotoSubscriber());
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ((ImageAdapter)imageGridView.getAdapter()).updateSearchData(flikrPhotosList,newText);
        imageGridView.getAdapter().notifyDataSetChanged();
        imageGridView.scrollToPosition(0);
        return true;
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch (id)
        {
            case R.id.api_button: {
                imageGridView.setVisibility(View.VISIBLE);
                apiLayout.setVisibility(View.GONE);
                 APIKey=apiEditText.getText().toString();
                flikrAPICall();
                break;
            }
        }
    }

    public class PhotoSubscriber extends Subscriber<Response<SearchResponse>>
    {


        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
                imageGridView.setVisibility(View.GONE);
                apiLayout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onNext(Response<SearchResponse> searchResponseResponse) {
            SearchResponse searchResponse=searchResponseResponse.body();
            flikrPhotosList.addAll(searchResponse.getPhotos().getPhoto());
            imageGridView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class ImageViewHolder  extends RecyclerView.ViewHolder{

        NetworkImageView imageView;
        public ImageViewHolder(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image);
        }
    }



    public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        List photoList=new ArrayList();

        public ImageAdapter(List photoList)
        {
            this.photoList=photoList;
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                View view= LayoutInflater.from(MainActivity.this).inflate(R.layout.image_adapter,parent,false);
                return new ImageViewHolder(view);


        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            Photo photo= (Photo) photoList.get(position);

                ((ImageViewHolder) holder).imageView.setScaleType(NetworkImageView.ScaleType.FIT_XY);
                ((ImageViewHolder) holder).imageView.setImageUrl("http://farm" + photo.getFarm()+ ".staticflickr.com/" + photo.getServer() +
                        "/" + photo.getId() + "_" + photo.getSecret() + ".jpg", CustomVolleyRequestQueue.getInstance(MainActivity.this).getImageLoader());

        }

        @Override
        public int getItemCount() {
            return photoList.size();
        }

        public void updateSearchData(List data,String searchText)
        {
                List searchResult=new ArrayList();
                for (Object obj:data){
                    Photo photo= (Photo) obj;
                    if (photo.getTitle().toLowerCase().contains(searchText.toLowerCase()))
                    {
                        searchResult.add(photo);
                    }
                }
                photoList=searchResult;
        }

    }


}
