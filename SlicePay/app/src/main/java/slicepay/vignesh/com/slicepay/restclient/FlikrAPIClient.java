package slicepay.vignesh.com.slicepay.restclient;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import slicepay.vignesh.com.slicepay.response.SearchResponse;



public interface FlikrAPIClient {
    @GET("services/rest/")
    Observable<Response<SearchResponse>> search(@Query("method") String method,@Query("page") int page, @Query("api_key") String apiKey, @Query("per_page") int perPage, @Query("format") String format, @Query("nojsoncallback") int nojsoncallback);
}


